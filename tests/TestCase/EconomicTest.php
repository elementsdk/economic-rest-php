<?php

namespace Elements\Economic\Test\TestCase;
use Elements\Economic\Economic;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use PHPUnit\Framework\TestCase;

/**
 * Class EconomicTest
 *
 * @package Elements\Economic\Test\TestCase
 */
class EconomicTest extends TestCase {

	/**
	 *
	 */
	public function setUp() {
		parent::setUp();
	}

	/**
	 *
	 */
	public function testInit() {

		try {
			$client = new Economic();
			$this->fail('No exception raised');
		} catch (\Exception $e) {
			$this->assertContains('Please provide appId and accessToken', $e->getMessage());
		}

		$client = new Economic('appId', 'AccessKey');
		$this->assertInstanceOf('Elements\Economic\Economic', $client);
		$this->assertObjectHasAttribute('client', $client);
		$this->assertInstanceOf('GuzzleHttp\Client', $client->client);

	}

	public function testSendRequest() {

		$mock   = new MockHandler([
			new Response(200, ['X-Foo' => 'Bar'], '{"some1":"value"}'),
			new Response(403, ['X-Foo' => 'Bar'], '{"some2":"value"}')

		]);
        $handler = HandlerStack::create($mock);

        $client = new Economic('appId', 'AccessKey', $handler);

		$out = $client->sendRequest('/customers?pagesize=1000', 'get');
		$this->assertEquals(['some1' => 'value'], $out);

		try {
			$out = $client->sendRequest('/customers?pagesize=1000', 'get');
			$this->fail('No exception raised');
		} catch (\Exception $e) {
			$this->assertContains('Access Denied', $e->getMessage());
		}
	}
}
