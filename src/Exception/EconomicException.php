<?php

namespace Elements\Economic\Exception;

class EconomicException extends \Exception
{

    public function __construct($message = null)
    {
        if (empty($message)) {
            $message = 'Unauthorized';
        }
        parent::__construct($message);
    }
}
