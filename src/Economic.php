<?php

namespace Elements\Economic;

use Elements\Economic\Exception\EconomicException;
use GuzzleHttp;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Economic
 *
 * @package Elements\Economic
 */
class Economic
{

    /**
     * @var string
     */
    private $appId;

    /**
     * @var string
     */
    private $accessId;

    /**
     * @var Client
     */
    public $client;


    /**
     * @param string $appId
     * @param string $accessId
     */
    public function __construct($appId = null, $accessId = null, $handler = null)
    {
        if (!$appId || !$accessId) {
            throw new EconomicException('Please provide appId and accessToken');
        }
        $this->appId    = $appId;
        $this->accessId = $accessId;

        $config = [
            'base_uri' => 'https://restapi.e-conomic.com',
            'headers'  => [
                'accept'   => 'application/json',
                'appId'    => $this->appId,
                'accessId' => $this->accessId
            ]
        ];

        if ($handler) {
            $config['handler'] = $handler;
        }

        /**
         * Client
         */
        $this->client = new Client($config);
    }

    /**
     * @param $endpoint
     * @param string $method
     * @param array $data
     * @param array $options
     */
    public function sendRequest($endpoint, $method = 'get', $options = [])
    {

        try {

            /**
             * @var $response ResponseInterface
             */
            $response = $this->client->{$method}($endpoint, $options);

            return json_decode((string)$response->getBody(), true);

        } catch (GuzzleHttp\Exception\ClientException $e) {
            throw new EconomicException('Request Error - ' . $e->getResponse()->getBody());
        }
    }

    /**
     * @param $url
     * @param $method
     * @param callable $callable
     * @return array
     */
    public function getAll($url, $method, $callable = null)
    {

        $result = $this->sendRequest($url, $method);

        if ($callable) {
            $return = call_user_func($callable, $result);
        } else {
            $return = $result['collection'];
        }

        if (!empty($result['pagination']['nextPage']) && $result['pagination']['nextPage'] !== $result['self']) {
            $more = $this->getAll($result['pagination']['nextPage'], $method, $callable);
            if (is_array($more)) {
                $return = array_merge($return, $more);
            }
        }

        return $return;
    }
}
